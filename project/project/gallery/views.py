from django.shortcuts import render, get_object_or_404
from .models import Item, Photo


def home(request):
    return render(request, 'home.html', {'item_list': Item.objects.all()[:2]})


def item_list(request):
    return render(request, 'item_list.html', {'item_list': Item.objects.all()})


def item_detail(request, object_id):
    _item_detail = get_object_or_404(Item, id = object_id)
    return render(request, 'item_detail.html', {'item_detail': _item_detail})


def photo_detail(request, object_id):
    _photo_detail = get_object_or_404(Photo, id = object_id)
    return render(request, 'photo_detail.html', {'photo_detail': _photo_detail})