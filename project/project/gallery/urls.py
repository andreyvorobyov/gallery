from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'items/$', views.item_list, name='items'),
    url(r'items/(?P<object_id>\d+)/$', views.item_detail, name='items'),
    url(r'photos/(?P<object_id>\d+)/$', views.photo_detail, name='photos'),
]